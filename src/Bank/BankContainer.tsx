import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {Routes} from './routes';
import Main from './main/main';
import Login from "./login/login";
import {getAppId} from "./util";
import styled from 'styled-components';

const SuperContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 0;
  flex-shrink: 0;
  height: 100vh;
  width: 100vw;
  justify-content: center;
  align-items: center;
  background: lightgray;
`;

const BankContainer: React.FC = () => {
  getAppId();
  return (
    <SuperContainer>
      <BrowserRouter>
        <Switch>
          <Route exact path={Routes.login} component={Login}/>
          <Route component={Main}/>
        </Switch>
      </BrowserRouter>
    </SuperContainer>
  );
};

export default BankContainer;
