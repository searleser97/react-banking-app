export default interface IPIProperty {
  name: string;
  value: number | string;
}