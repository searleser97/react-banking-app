import IPIProperty from "./IPIProperty";

export default interface IPISignal {
  name: string;
  properties: IPIProperty[];
  datetime: string;
}