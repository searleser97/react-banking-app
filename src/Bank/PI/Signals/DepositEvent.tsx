import IPISignal from "../IPISignal";
import IPIProperty from "../IPIProperty";

export class DepositEvent implements IPISignal {

  name: string = "deposit_event";
  datetime: string = new Date().toLocaleString();

  properties: IPIProperty[] = [
    {name: DepositEvent.propertyNames.channel, value: ""},
    {name: DepositEvent.propertyNames.amount, value: 0},
    {name: DepositEvent.propertyNames.mode, value: ""}
  ];

  constructor(channel: string, amount: number, mode: string) {
    this.properties[0].value = channel;
    this.properties[1].value = amount;
    this.properties[2].value = mode;
  }

  static propertyNames = {
    channel: "Channel",
    amount: "Amount",
    mode: "Mode",
  };

  static indexForProperty = new Map<string, number>(
    [
      [DepositEvent.propertyNames.channel, 0],
      [DepositEvent.propertyNames.amount, 1],
      [DepositEvent.propertyNames.mode, 2]
    ]
  );

}