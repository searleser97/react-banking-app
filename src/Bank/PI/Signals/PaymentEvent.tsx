import IPISignal from "../IPISignal";
import IPIProperty from "../IPIProperty";

export class PaymentEvent implements IPISignal {

  name: string = "payment_event";
  datetime: string = new Date().toLocaleString();

  properties: IPIProperty[] = [
    {name: PaymentEvent.propertyNames.channel, value: ""},
    {name: PaymentEvent.propertyNames.amount, value: 0},
    {name: PaymentEvent.propertyNames.payee, value: ""}
  ];

  constructor(channel: string, amount: number, payee: string) {
    this.properties[0].value = channel;
    this.properties[1].value = amount;
    this.properties[2].value = payee;
  }

  static propertyNames = {
    channel: "Channel",
    amount: "Amount",
    payee: "Payee",
  };

  static indexForProperty = new Map<string, number>(
    [
      [PaymentEvent.propertyNames.channel, 0],
      [PaymentEvent.propertyNames.amount, 1],
      [PaymentEvent.propertyNames.payee, 2]
    ]
  );

}