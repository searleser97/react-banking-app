import IPISignal from "../IPISignal";
import IPIProperty from "../IPIProperty";

export class TransferEvent implements IPISignal {

  name: string = "transfer_event";
  datetime: string = new Date().toLocaleString();

  properties: IPIProperty[] = [
    {name: TransferEvent.propertyNames.channel, value: ""},
    {name: TransferEvent.propertyNames.amount, value: 0},
    {name: TransferEvent.propertyNames.fromAccount, value: ""},
    {name: TransferEvent.propertyNames.toAccount, value: ""}
  ];

  constructor(channel: string, amount: number, fromAccount: string, toAccount: string) {
    this.properties[0].value = channel;
    this.properties[1].value = amount;
    this.properties[2].value = fromAccount;
    this.properties[3].value = toAccount;
  }

  static propertyNames = {
    channel: "Channel",
    amount: "Amount",
    fromAccount: "FromAccount",
    toAccount: "ToAccount"
  };

  static indexForProperty = new Map<string, number>(
    [
      [TransferEvent.propertyNames.channel, 0],
      [TransferEvent.propertyNames.amount, 1],
      [TransferEvent.propertyNames.fromAccount, 2],
      [TransferEvent.propertyNames.toAccount, 3]
    ]
  );

}