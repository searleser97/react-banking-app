import {makeStyles} from '@material-ui/core/styles';

export const styles = makeStyles(theme => ({
  root: {
    display: 'flex',
    paddingTop: '10px',
    paddingBottom: '10px',
    alignItems: 'center',
    justifyContent: 'center'
  },
  content: {
    display: 'flex',
    width: '90%'
  },
  details: {
    display: 'flex',
    width: '90%',
    flexDirection: 'column'
  },
  arrowIcon: {
    display: 'flex',
    width: '10%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  accountName: {
    display: 'flex',
    flex: '1',
    alignItems: 'center'
  },
  accountAmount: {
    display: 'flex',
    flex: '1',
    alignItems: 'center'
  }
}));