import React from 'react';
import {styles} from './account-card.style';
import {Icon} from 'office-ui-fabric-react/lib/Icon';
import {Paper} from '@material-ui/core';
import {Label} from 'office-ui-fabric-react';
import NumberFormat from 'react-number-format';

export interface IAccountCard {
  accountName: string;
  accountBalance: number;
}

const AccountCard: React.FC<IAccountCard> = (props) => {
  const classes = styles(props);
  return (
    <Paper className={classes.root}>
      <div className={classes.content}>
        <div className={classes.details}>
          <Label className={classes.accountName}>{props.accountName}</Label>
          <div className={classes.accountAmount}>
            Balance:&nbsp;
            <NumberFormat value={props.accountBalance} displayType={'text'} thousandSeparator={true} prefix={'$'}/>
          </div>
        </div>
        <div className={classes.arrowIcon}>
          <Icon iconName="ChevronRight"/>
        </div>
      </div>
    </Paper>
  )
};

export default AccountCard;