import React from 'react';
import {Label} from 'office-ui-fabric-react';
import {Icon} from 'office-ui-fabric-react/lib/Icon';
import {styles} from './account-list.style';
import {Stack} from 'office-ui-fabric-react/lib/Stack';
import AccountCard from '../account-card/account-card';
import {Routes} from "../routes";
import {Link} from "react-router-dom";
import {getCurrentUserBalanceForAccount} from "../util";
import {ACCOUNTS_KEY} from "../constants";

const AccountList: React.FC = () => {
  const classes = styles();
  return (
    <div className={classes.root}>
      <Label
        style={{padding: '15px', width: '80%', textAlign: 'center', fontSize: '20px', flexDirection: 'column'}}>
        <Icon iconName="AllCurrency" style={{fontSize: '70px'}}/>
      </Label>
      <br/>
      <br/>
      <Stack style={{width: '100%'}} tokens={{childrenGap: 5}}>
        <Link to={Routes.accountDetails + "/checking"} className={classes.link}>
          <AccountCard accountName="Checking" accountBalance={getCurrentUserBalanceForAccount(ACCOUNTS_KEY.checking)}/>
        </Link>
        <Link to={Routes.accountDetails + "/savings"} className={classes.link}>
          <AccountCard accountName="Savings" accountBalance={getCurrentUserBalanceForAccount(ACCOUNTS_KEY.savings)}/>
        </Link>
        <Link to={Routes.accountDetails + "/credit"} className={classes.link}>
          <AccountCard accountName="Credit" accountBalance={getCurrentUserBalanceForAccount(ACCOUNTS_KEY.credit)}/>
        </Link>
      </Stack>
    </div>
  )
};

export default AccountList;