import {makeStyles} from '@material-ui/core/styles';

export const styles = makeStyles(theme => ({
  root: {
    display: 'flex',
    paddingTop: '10px',
    paddingBottom: '10px',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%'
  },
  content: {
    display: 'flex',
    width: '85%',
    justifyContent: 'space-between'
  },
  details: {
    display: 'flex',
    flexDirection: 'column'
  },
  arrowIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  accountName: {
    display: 'flex',
    flex: '1',
    alignItems: 'center'
  },
  accountAmount: {
    display: 'flex',
    flex: '1',
    alignItems: 'center'
  }
}));