import React from 'react';
import {Label} from "office-ui-fabric-react";
import {Paper} from "@material-ui/core";
import {styles} from "./activity-details-card.styles";
import NumberFormat from "react-number-format";

export interface IActivityDetailsCard {
  date: string;
  operationDescription: string;
  operationCost: number;
  accountBalance: number;
}

const ActivityDetailsCard: React.FC<IActivityDetailsCard> = (props) => {
  const classes = styles();
  return (
    <Paper className={classes.root}>
      <div className={classes.content}>
        <div className={classes.details}>
          <Label className={classes.accountName}>{props.date}</Label>
          <div className={classes.accountAmount}>
            {props.operationDescription}
          </div>
        </div>
        <div className={classes.arrowIcon}>
          <div style={{color: '#0078d4'}}>
            <NumberFormat value={props.operationCost} displayType={'text'} thousandSeparator={true} prefix={'$'}/>
          </div>
          <Label>
            <NumberFormat value={props.accountBalance} displayType={'text'} thousandSeparator={true} prefix={'$'}/>
          </Label>
        </div>
      </div>
    </Paper>
  )
};

export default ActivityDetailsCard;