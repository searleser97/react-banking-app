import styled from 'styled-components';
import makeStyles from "@material-ui/core/styles/makeStyles";

export const AccountDetailsListContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    background: red;
    flex-direction: column;
`;

export const PaperContent = styled.div`
    display: flex;
    width: 90%;
    padding-top: 10px;
    padding-bottom: 10px;
    flex-direction: column;
`;

export const styles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexShrink: 1,
    width: '90%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  stack: {
    width: '100%',
    overflowY: 'auto',
    maxHeight: '50vh',
    borderRadius: '10px'
  },
  link: {
    outline: 'none',
    textDecoration: 'none',
    '&:link': {
      textDecoration: 'none'
    },
    '&:visited': {
      textDecoration: 'none'
    },
    '&:hover': {
      textDecoration: 'none'
    },
    '&:active': {
      textDecoration: 'none'
    }
  }
}));