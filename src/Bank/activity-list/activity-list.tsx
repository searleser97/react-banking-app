import React from 'react';
import {withRouter} from "react-router";
import {ACCOUNTS_KEY, Activity} from "../constants";
import ActivityDetailsCard from "../activity-details-card/activity-details-card";
import {PaperContent, styles} from "./activity-list.style";
import {Stack} from 'office-ui-fabric-react/lib/Stack';
import {Paper} from "@material-ui/core";
import {Label} from "office-ui-fabric-react";
import {getCurrentUserBalanceForAccount, getCurrentUserHistoryForAccount, getRandomInt} from "../util";
import {ACCOUNT_DETAILS_PARAM} from "../routes";


const accountNameForAccountId = new Map();


export const ComponentWithRouterParams = withRouter(props => {
    accountNameForAccountId.set(ACCOUNTS_KEY.checking, "Checking");
    accountNameForAccountId.set(ACCOUNTS_KEY.savings, "Savings");
    accountNameForAccountId.set(ACCOUNTS_KEY.credit, "Credit");

    const classes = styles();
    const accountId = props.match.params[ACCOUNT_DETAILS_PARAM];
    const activities: Activity[] = getCurrentUserHistoryForAccount(accountId);
    const cards = activities.reverse().map(activity => {
      return (
        <ActivityDetailsCard
          key={activity.date + activity.description + getRandomInt(0, 1000)}
          date={activity.date}
          operationDescription={activity.description}
          operationCost={activity.operationCost}
          accountBalance={activity.balance}
        />
      );
    });
    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <PaperContent>
            <div style={{display: 'flex', width: '100%'}}>
              {accountNameForAccountId.get(accountId)}
            </div>
            <div style={{display: 'flex', width: '100%'}}>
              <Label>Balance: ${getCurrentUserBalanceForAccount(accountId)}</Label>
            </div>
          </PaperContent>
        </Paper>
        <br/>
        <br/>
        <Stack tokens={{childrenGap: 6}} className={classes.stack}>
          {cards}
        </Stack>
      </div>
    )
  }
);

const ActivityList: React.FC = () => {

  return (
    <ComponentWithRouterParams/>
  )
};

export default ActivityList;