import React, {useState} from 'react';
import {FormContainer, styles} from './bill-pay.style';
import {Dropdown, IDropdownOption} from 'office-ui-fabric-react/lib/Dropdown';
import {TextField} from 'office-ui-fabric-react/lib/TextField';
import {Label, PrimaryButton} from 'office-ui-fabric-react';
import {Icon} from 'office-ui-fabric-react/lib/Icon';
import {PaymentEvent} from "../PI/Signals/PaymentEvent";
import {addAccountActivityToCurrentUser, EventEmitter, getCurrentUser, saveAndSendSignal} from "../util";
import {ACCOUNTS_KEY} from "../constants";

const BillPay: React.FC = () => {

  const buttonRef = React.useRef(null);

  const payees: IDropdownOption[] = [
    {key: "electricity", text: 'Electricity'},
    {key: "water", text: 'Water'},
    {key: "rent", text: 'Rent'},
    {key: "automobileemi", text: 'Automobile EMI'}
  ];

  const fromOptions: IDropdownOption[] = [];
  const userAccounts = getCurrentUser().accounts;
  for (let account of userAccounts) {
    fromOptions.push({key: account.type, text: account.name + " - $" + account.balance});
  }

  const saveAndSend = (accountType: string, payee: string, amount: number) => {
    EventEmitter.emitEvent('coachmark', buttonRef);
    let signal = new PaymentEvent("App", amount, payee);
    addAccountActivityToCurrentUser(accountType, -amount, "Paid " + payee);
    saveAndSendSignal(signal);
  };
  const [amount, setAmount] = useState('0');
  const [payee, setPayee] = useState('water');
  const [accountType, setAccountType] = useState(ACCOUNTS_KEY.checking);
  const classes = styles();
  return (
    <div className={classes.root}>
      <Label
        style={{padding: '15px', width: '80%', textAlign: 'center', fontSize: '20px', flexDirection: 'column'}}>
        <Icon iconName="Money" style={{fontSize: '70px'}}/>
      </Label>
      <FormContainer>
        <Dropdown onChange={(event, option: IDropdownOption | undefined, index) => setPayee(option ? option.text : '')}
                  placeholder="Select payee" label="Payee" options={payees}/>
        <Dropdown
          onChange={(event, option: IDropdownOption | undefined, index) => setAccountType(option ? option.key as string : '')}
          placeholder="Select an account" label="From" options={fromOptions}/>
        <TextField onChange={(event, newValue) => {
          setAmount(newValue as string)
        }} type="number" label="Amount"/>
        <br/>
        <br/>
        <div ref={buttonRef} className={classes.buttonContainer}>
          <PrimaryButton className={classes.button} onClick={() => saveAndSend(accountType, payee, Number(amount))} text="Pay Bill"/>
        </div>
      </FormContainer>
    </div>
  )
};

export default BillPay;