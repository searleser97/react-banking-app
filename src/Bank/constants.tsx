import hannahImg from './persona-list/assets/hannah.png';
import aliImg from './persona-list/assets/ali.png';
import donnaImg from './persona-list/assets/donna.png';
import johnImg from './persona-list/assets/john.png';
import alexImg from './persona-list/assets/alex.png';
import zachImg from './persona-list/assets/zach.png';

export const ACCOUNTS_KEY = {
  checking: "checking",
  savings: "savings",
  credit: "credit"
};

export const ACCOUNTS_NAME = {
  checking: "Checking",
  savings: "Savings",
  credit: "Credit"
};

export const INITIAL_BALANCE = {
  hannah: {
    checking: 100000,
    savings: 200000,
    credit: 5000
  },
  ali: {
    checking: 3000000,
    savings: 5000000,
    credit: 50000
  },
  donna: {
    checking: 120000,
    savings: 1000000,
    credit: 10000
  },
  john: {
    checking: 300000,
    savings: 1000000,
    credit: 3000
  },
  alex: {
    checking: 1000,
    savings: 2000,
    credit: 500
  },
  zach: {
    checking: 5000,
    savings: 10000,
    credit: 1000
  }
};

export interface Activity {
  balance: number;
  operationCost: number;
  description: string;
  date: string;
}

export interface Account {
  name: string;
  type: string;
  balance: number;
  history: Activity[];
}

export interface User {
  id: number;
  name: string;
  age: number;
  city: string;
  state: string;
  income: number;
  employment: string;
  profileImg: string;
  accounts: Account[];
}

export const USERS: Map<number, User> = new Map<number, User>(
  [
    [1, {
      id: 1, name: 'Hannah', age: 50, city: 'Austin', state: 'TX', income: 80, employment: 'Employed',
      profileImg: hannahImg,
      accounts: [
        {
          name: ACCOUNTS_NAME.checking,
          type: ACCOUNTS_KEY.checking,
          balance: INITIAL_BALANCE.hannah.checking,
          history: [
            {balance: INITIAL_BALANCE.hannah.checking, operationCost: INITIAL_BALANCE.hannah.checking, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        },
        {
          name: ACCOUNTS_NAME.savings,
          type: ACCOUNTS_KEY.savings,
          balance: INITIAL_BALANCE.hannah.savings,
          history: [
            {balance: INITIAL_BALANCE.hannah.savings, operationCost: INITIAL_BALANCE.hannah.savings, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        },
        {
          name: ACCOUNTS_NAME.credit,
          type: ACCOUNTS_KEY.credit,
          balance: INITIAL_BALANCE.hannah.credit,
          history: [
            {balance: INITIAL_BALANCE.hannah.credit, operationCost: INITIAL_BALANCE.hannah.credit, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        }
      ]
    }],
    [2, {
      id: 2,
      name: "Ali", age: 34, city: 'Berverly Hills', state: 'CA', income: 1000, employment: 'Self-Employed',
      profileImg: aliImg,
      accounts: [
        {
          name: ACCOUNTS_NAME.checking,
          type: ACCOUNTS_KEY.checking,
          balance: INITIAL_BALANCE.ali.checking,
          history: [
            {balance: INITIAL_BALANCE.ali.checking, operationCost: INITIAL_BALANCE.ali.checking, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        },
        {
          name: ACCOUNTS_NAME.savings,
          type: ACCOUNTS_KEY.savings,
          balance: INITIAL_BALANCE.ali.savings,
          history: [
            {balance: INITIAL_BALANCE.ali.savings, operationCost: INITIAL_BALANCE.ali.savings, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        },
        {
          name: ACCOUNTS_NAME.credit,
          type: ACCOUNTS_KEY.credit,
          balance: INITIAL_BALANCE.ali.credit,
          history: [
            {balance: INITIAL_BALANCE.ali.credit, operationCost: INITIAL_BALANCE.ali.credit, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        }
      ]
    }],
    [3, {
      id: 3, name: "Donna", age: 42, city: 'Chicago', state: 'IL', income: 300, employment: 'Employed',
      profileImg: donnaImg,
      accounts: [
        {
          name: ACCOUNTS_NAME.checking,
          type: ACCOUNTS_KEY.checking,
          balance: INITIAL_BALANCE.donna.checking,
          history: [
            {balance: INITIAL_BALANCE.donna.checking, operationCost: INITIAL_BALANCE.donna.checking, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        },
        {
          name: ACCOUNTS_NAME.savings,
          type: ACCOUNTS_KEY.savings,
          balance: INITIAL_BALANCE.donna.savings,
          history: [
            {balance: INITIAL_BALANCE.donna.savings, operationCost: INITIAL_BALANCE.donna.savings, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        },
        {
          name: ACCOUNTS_NAME.credit,
          type: ACCOUNTS_KEY.credit,
          balance: INITIAL_BALANCE.donna.credit,
          history: [
            {balance: INITIAL_BALANCE.donna.credit, operationCost: INITIAL_BALANCE.donna.credit, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        }
      ]
    }],
    [4, {
      id: 4, name: "John", age: 65, city: 'Hudson', state: 'OH', income: 35, employment: 'Retired',
      profileImg: johnImg,
      accounts: [
        {
          name: ACCOUNTS_NAME.checking,
          type: ACCOUNTS_KEY.checking,
          balance: INITIAL_BALANCE.john.checking,
          history: [
            {balance: INITIAL_BALANCE.john.checking, operationCost: INITIAL_BALANCE.john.checking, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        },
        {
          name: ACCOUNTS_NAME.savings,
          type: ACCOUNTS_KEY.savings,
          balance: INITIAL_BALANCE.john.savings,
          history: [
            {balance: INITIAL_BALANCE.john.savings, operationCost: INITIAL_BALANCE.john.savings, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        },
        {
          name: ACCOUNTS_NAME.credit,
          type: ACCOUNTS_KEY.credit,
          balance: INITIAL_BALANCE.john.credit,
          history: [
            {balance: INITIAL_BALANCE.john.credit, operationCost: INITIAL_BALANCE.john.credit, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        }
      ]
    }],
    [5, {
      id: 5, name: "Alex", age: 17, city: 'Syracuse', state: 'NY', income: 2, employment: 'Student',
      profileImg: alexImg,
      accounts: [
        {
          name: ACCOUNTS_NAME.checking,
          type: ACCOUNTS_KEY.checking,
          balance: INITIAL_BALANCE.zach.checking,
          history: [
            {balance: INITIAL_BALANCE.zach.checking, operationCost: INITIAL_BALANCE.zach.checking, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        },
        {
          name: ACCOUNTS_NAME.savings,
          type: ACCOUNTS_KEY.savings,
          balance: INITIAL_BALANCE.zach.savings,
          history: [
            {balance: INITIAL_BALANCE.zach.savings, operationCost: INITIAL_BALANCE.zach.savings, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        },
        {
          name: ACCOUNTS_NAME.credit,
          type: ACCOUNTS_KEY.credit,
          balance: INITIAL_BALANCE.zach.credit,
          history: [
            {balance: INITIAL_BALANCE.zach.credit, operationCost: INITIAL_BALANCE.zach.credit, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        }
      ]
    }],
    [6, {
      id: 6,
      name: "Zach", age: 21, city: 'Ann Arbor', state: 'MI', income: 10, employment: 'Student',
      profileImg: zachImg,
      accounts: [
        {
          name: ACCOUNTS_NAME.checking,
          type: ACCOUNTS_KEY.checking,
          balance: INITIAL_BALANCE.alex.checking,
          history: [
            {balance: INITIAL_BALANCE.alex.checking, operationCost: INITIAL_BALANCE.alex.checking, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        },
        {
          name: ACCOUNTS_NAME.savings,
          type: ACCOUNTS_KEY.savings,
          balance: INITIAL_BALANCE.alex.savings,
          history: [
            {balance: INITIAL_BALANCE.alex.savings, operationCost: INITIAL_BALANCE.alex.savings, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        },
        {
          name: ACCOUNTS_NAME.credit,
          type: ACCOUNTS_KEY.credit,
          balance: INITIAL_BALANCE.alex.credit,
          history: [
            {balance: INITIAL_BALANCE.alex.credit, operationCost: INITIAL_BALANCE.alex.credit, description: "Initial Balance", date: new Date().toLocaleString()}
          ],
        }
      ]
    }]
  ]
);

