import {makeStyles} from '@material-ui/core/styles';
import styled from 'styled-components';

export const styles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexShrink: 1,
    width: '90%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonContainer: {
    display: 'flex',
    flex: '1',
    background: 'red'
  },
  button: {
    display: 'flex',
    flex: '1',
    justifyContent: 'center'
  }
}));

export const FormContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`;

export const Title = styled.div`
    display: flex;
    font-size: 20px;
`;