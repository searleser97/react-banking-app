import React, {useState} from 'react';
import {FormContainer, styles} from './deposit.style';
import {Dropdown, IDropdownOption} from 'office-ui-fabric-react/lib/Dropdown';
import {TextField} from 'office-ui-fabric-react/lib/TextField';
import {Label, PrimaryButton} from 'office-ui-fabric-react';
import {ACCOUNTS_KEY} from '../constants';
import {Icon} from 'office-ui-fabric-react/lib/Icon';
import {addAccountActivityToCurrentUser, EventEmitter, getCurrentUser, saveAndSendSignal} from "../util";
import {DepositEvent} from "../PI/Signals/DepositEvent";

const Deposit: React.FC = () => {
  const classes = styles();
  const buttonRef = React.useRef(null);
  const saveAndSend = (accountType: string, amount: number) => {
    EventEmitter.emitEvent('coachmark', buttonRef);
    let signal = new DepositEvent("App", amount, "Cheque");
    addAccountActivityToCurrentUser(accountType, amount, "Received Direct Deposit of $" + amount);
    saveAndSendSignal(signal);
  };

  const toOptions: IDropdownOption[] = [];
  const userAccounts = getCurrentUser().accounts;
  for (let account of userAccounts) {
    toOptions.push({key: account.type, text: account.name + " - $" + account.balance});
  }

  const [amount, setAmount] = useState('0');
  const [accountType, setAccountType] = useState(ACCOUNTS_KEY.checking);

  return (
    <div className={classes.root}>
      <Label
        style={{padding: '15px', width: '80%', textAlign: 'center', fontSize: '20px', flexDirection: 'column'}}>
        <Icon iconName="PreviewLink" style={{fontSize: '70px'}}/>
      </Label>
      <FormContainer>
        <Dropdown
          onChange={(event, option: IDropdownOption | undefined, index) => setAccountType(option ? option.key as string : '')}
          placeholder="Select an account" label="To" options={toOptions}/>
        <TextField onChange={(event, newValue) => {
          setAmount(newValue as string)
        }} type="number" label="Amount"/>
        <br/>
        <br/>
        <div ref={buttonRef} className={classes.buttonContainer}>
          <PrimaryButton className={classes.button} onClick={() => saveAndSend(accountType, Number(amount))}
                         text="Deposit"/>
        </div>
      </FormContainer>
    </div>
  )
};

export default Deposit;