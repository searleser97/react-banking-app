import styled, {keyframes} from 'styled-components';
import {ISignalsIconContainer} from "./footer";
import {makeStyles} from "@material-ui/core";

export const styles = makeStyles(theme => ({
  front: {
    display: 'flex',
    position: 'absolute',
    backfaceVisibility: 'hidden'
  },
  back: {
    display: 'flex',
    position: 'absolute',
    backfaceVisibility: 'hidden',
    transform: 'rotateX(180deg)',
  }
}));

export const FooterContainer = styled.div`
    display: flex;
    flex: 0 0 auto;
    width: 100%;
    height: 7vh;
    align-items: center;
    justify-content: center;
    align-content: center;
    outline: none;
    
`;

export const BottomShape = styled.div`
  display: flex;
  width: 90%;
  height: 100%;
  background: #0078d4;
  align-items: center;
  justify-content: flex-end;
  cursor: pointer;
  outline: none;
  border-radius: 50px 50px 0 0;
`;

export const flipForward = keyframes`
  from {
    transform: rotateX(0deg);
    height: 4vh;
  }
  to {
    transform: rotateX(180deg);
    height: 4vh;
  }
`;

export const flipBackwards = keyframes`
  from {
    transform: rotateX(180deg);
    height: 4vh;
  }
  to {
    transform: rotateX(0deg);
    height: 4vh;
  }
`;

export const HorizontalSpace = styled.div`
  height: 100%;
  width: 20px;
  display: flex;
`;

export const SignalsIconContainer = styled.div<ISignalsIconContainer>`
  display: flex;
  width: 50px;
  height: 100%;
  color: white;
  font-size: 30px;
  align-items: center;
  align-content: center;
  justify-content: center;
  animation-name: ${props => {
    if (props.unblink) return UnBlink;
    if (props.blink) return Blink;
    return props.runAnimation ? props.runAnimationForward ? flipForward : flipBackwards : ''
  }};
  animation-duration: ${props => props.blink || props.unblink ? '200ms' : '1000ms'};
  animation-delay: 0ms;
  animation-iteration-count: 1;
  animation-fill-mode: forwards;
  transform-style: preserve-3d;
  perspective: 1000px;
`;

export const SignalCounter = styled.div`
  display: flex;
  width: 30px;
  color: white;
  font-weight: 400;
  font-size: 22px;
`;

export const Text = styled.div`
  display: flex;
  flex: 1;
  color: white;
  font-weight: 300;
  font-size: 20px;
  align-content: center;
  align-items: center;
`;
export const Blink = keyframes`
  from {
    font-size: 30px;
    color: white;
  }
  to {
    font-size: 40px;
    color: lightgreen;
  }
`;

export const UnBlink = keyframes`
  from {
    font-size: 40px;
    color: lightgreen;
  }
  to {
    font-size: 30px;
    color: white;
  }
`;