import React, {useEffect, useState} from 'react';
import {
  BottomShape,
  FooterContainer,
  HorizontalSpace,
  SignalCounter,
  SignalsIconContainer,
  styles
} from "./footer.style";
import {Icon} from 'office-ui-fabric-react/lib/Icon';
import {EventEmitter, getAllSignals} from "../util";
import {Text} from "./footer.style";

export interface IFooter {
  onTouched: () => void;
  runAnimation: boolean;
}

export interface ISignalsIconContainer {
  runAnimationForward: boolean;
  runAnimation: boolean;
  blink: boolean;
  unblink: boolean;
}

const Footer: React.FC<IFooter> = (props) => {
  const classes = styles();
  const [runAnimationForward, setAnimationDirection] = useState(true);
  const [blink, setBlink] = useState(false);
  const [unblink, setUnBlink] = useState(false);
  const [runAnimation, setRunAnimation] = useState(false);
  useEffect(() => setRunAnimation(props.runAnimation), [props.runAnimation]);


  EventEmitter.subscribeEvent("blink",
    (data) => {
      setBlink(true);
      setUnBlink(false);
      setTimeout(() => {
        setBlink(false);
        setUnBlink(true);
      }, 400);
    }
  );
  return (
    <FooterContainer>
      <BottomShape onClick={() => {
        props.onTouched();
        setAnimationDirection(!runAnimationForward)
      }}>
        <HorizontalSpace/>
        <HorizontalSpace/>
        <Text>Show Signals</Text>
        <HorizontalSpace/>
        <SignalCounter>
          ({getAllSignals().length})
        </SignalCounter>
        <HorizontalSpace/>
        <SignalsIconContainer unblink={unblink} blink={blink} runAnimation={runAnimation}
                              runAnimationForward={runAnimationForward}>
          <div className={classes.front}>
            <Icon style={{fontWeight: 600}} iconName={"Streaming"}/>
          </div>
          <div className={classes.back}>
            <Icon iconName={"ChevronDown"}/>
          </div>
          {/*<Icon iconName="InternetSharing"/>*/}
          {/*<Icon iconName="Streaming"/>*/}
          {/*<Icon iconName="Communications"/>*/}
        </SignalsIconContainer>
        <HorizontalSpace/>
        <HorizontalSpace/>
      </BottomShape>
    </FooterContainer>
  );
};


export default Footer;