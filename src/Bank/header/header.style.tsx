import styled from 'styled-components';
import {makeStyles} from "@material-ui/core";

export const styles = makeStyles(theme => ({
  link: {
    outline: 'none',
    textDecoration: 'none',
    '&:link': {
      textDecoration: 'none'
    },
    '&:visited': {
      textDecoration: 'none'
    },
    '&:hover': {
      textDecoration: 'none'
    },
    '&:active': {
      textDecoration: 'none'
    },
    color: 'white'
  }
}));

export const HeaderContainer = styled.div`
  display: flex;
  width: 100%;
  background: #0078d4;
  justify-content: space-between;
  height: 75%;
`;

export const RootContainer = styled.div`
  display: flex;
  flex: 0 0 auto;
  width: 100%;
  height: 12%;
  flex-direction: column;
  justify-content: space-between;
`;

export const InfoContainer = styled.div`
  display: flex;
  width: 100%;
  flex: 1;
  justify-content: center;
  align-content: center;
  align-items: center;
`;

export const Info = styled.div`
  background: gray;
  height: 100%;
  width: 70%;
  border-radius: 0 0 50px 50px;
  text-align: center;
  display: flex;
  flex-direction: column;
  color: white;
`;

export const IconContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 60px;
  height: 100%;
`;

export const LeftIcon = styled(IconContainer)`
  font-size: 1.5rem;
  color: white;
  font-weight: bolder;
  cursor: pointer;
`;

export const RightIcon = styled(IconContainer)`
  font-size: 1.5rem;
  color: white;
  font-weight: bolder;
`;

export interface IImgIcon {
  imgUrl: string
}

export const ImgIcon = styled.div<IImgIcon>`
  display: flex;
  width: 35px;
  height: 35px;
  background-image: url(${props => props.imgUrl});
  background-size: 100% 100%;
`;