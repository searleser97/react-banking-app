import React, {useState} from 'react';
import {Icon} from 'office-ui-fabric-react/lib/Icon';
import {
  HeaderContainer,
  IconContainer,
  ImgIcon,
  Info,
  InfoContainer,
  LeftIcon,
  RightIcon,
  RootContainer,
  styles
} from './header.style';
import {Link, Redirect, withRouter} from 'react-router-dom';
import {Routes} from "../routes";
import {getAppId, getCurrentUserProfileImgUrl} from "../util";

export const Back = withRouter(props =>
  <LeftIcon>
    <Icon iconName="Back" onClick={() => props.history.goBack()}/>
  </LeftIcon>
);


const Header: React.FC = () => {
  const classes = styles();
  const [goToLogin, setGoToLogin] = useState(false);
  if (goToLogin) return <Redirect push to={Routes.login}/>;
  return (
    <RootContainer>
      <HeaderContainer>
        <Back/>
        <IconContainer>
          <ImgIcon onClick={() => setGoToLogin(true)} imgUrl={getCurrentUserProfileImgUrl()}/>
        </IconContainer>
        <RightIcon>
          <Link className={classes.link} to={Routes.settings}>
            <div><Icon iconName="Settings"/></div>
          </Link>
        </RightIcon>
      </HeaderContainer>
      <InfoContainer>
        <Info>
          APP ID: {getAppId()}
        </Info>
      </InfoContainer>
    </RootContainer>
  )
};

export default Header;