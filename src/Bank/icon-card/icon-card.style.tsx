import styled from 'styled-components';
import Paper from '@material-ui/core/Paper';
import {Icon} from 'office-ui-fabric-react/lib/Icon';

export const Background = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
    align-items: center;
    justify-content: center;
    border-width: 5px;
    border-color: black;
`;

export const MyIcon = styled(Icon)`
    width: fit-content;
    font-size: 5rem;
`;

export const Text = styled.div`
`;

export const MyPaper = styled(Paper)`
    text-align: center;
    padding: 25px 15px;
    @media (min-width: 500px) { /* laptop and tablets */
      padding: 30px 15px;
    }
`;