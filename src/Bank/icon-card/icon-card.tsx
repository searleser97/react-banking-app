import React from 'react';
import {Background, MyIcon, MyPaper, Text} from './icon-card.style';

export interface IIconCard {
  iconName: string;
  text: string;
}

const IconCard: React.FC<IIconCard> = (props: IIconCard) => {
  return (
    <MyPaper>
      <Background>
        <MyIcon iconName={props.iconName}/>
        <Text>{props.text}</Text>
      </Background>
    </MyPaper>
  )
};

export default IconCard;