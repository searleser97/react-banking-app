import styled from 'styled-components';
import msft from './assets/msft.jpg';

export const MessageBox = styled.div`
  display: flex;
  flex-grow: 0;
  flex-shrink: 0;
  background-color: #0078d4;
  color: white;
  font-size: 1.2rem;
  padding: 20px;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  background: white;
  @media (min-width: 500px) { /* laptop and tablets */
      width: 500px;
  }
`;

export const LogoContainer = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const LogoBox = styled.div`
  display: flex;
  flex: 0 1 auto;
  width: 40%;
  background-image: url(${msft});
  background-size: 100% 100%;
  &:before {
    content: "";
    display: block;
    padding-bottom: 100%;
  }
`;

// {/*<div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100vw', height: '100vh'}}>*/}