import React, {useState} from 'react';
import {Container, LogoBox, LogoContainer, MessageBox} from './login.style';
import {Redirect} from 'react-router-dom';
import {Routes} from "../routes";
import PersonaList, {Item} from "../persona-list/persona-list";
import {User, USERS} from "../constants";
import {PersonaSize} from "office-ui-fabric-react";
import {getUsers, saveUsers, setCurrentUserId} from "../util";


const Login: React.FC = () => {
  const [userHasBeenSelected, setUserHasBeenSelected] = useState(false);

  const setCurrentUser = (id: number): void => {
    setCurrentUserId(id);
    const users: (User | undefined)[] = getUsers();
    if (!users[id]) users[id] = USERS.get(id);
    saveUsers(users);
    setUserHasBeenSelected(true);
  };

  const items: Item[] = [];
  USERS.forEach((user) => {
    items.push({
      id: user.id,
      imageUrl: user.profileImg,
      imageInitials: user.name.substr(0, 2).toUpperCase(),
      text: user.name + ", " + user.age + " years",
      secondaryText: user.city + ', ' + user.state,
      tertiaryText: '$' + user.income + 'K/year',
      optionalText: user.employment,
      size: PersonaSize.size100
    });
  });

  return (
    <Container>
      <LogoContainer>
        <LogoBox/>
      </LogoContainer>
      <MessageBox>Select a user account to login</MessageBox>
      <PersonaList items={items} onItemSelected={setCurrentUser}/>
      {
        userHasBeenSelected && <Redirect push to={Routes.dashboard}/>
      }
    </Container>
  );
};

export default Login;