import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    align-content: center;
    width: 100%;
    height: 100%;
    background: whitesmoke;
    align-self: center;
    justify-self: center;
`;

export const MainContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    width: 100%;
    height: 100%;
    background: whitesmoke;
    @media (min-width: 500px) { /* laptop and tablets */
      width: 500px;
    }
`;