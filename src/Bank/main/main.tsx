import React, {useState} from 'react';
import {Redirect, Route} from "react-router-dom";
import Header from '../header/header';
import Footer from '../footer/footer';
import SimpleDashboard from '../simple-dashboard/simple-dashboard';
import Transfer from '../transfer/transfer';
import Deposit from '../deposit/deposit';
import BillPay from '../bill-pay/bill-pay'
import AccountList from '../account-list/account-list';
import {ACCOUNT_DETAILS_PARAM, Routes} from '../routes';
import {Container, MainContainer} from './main.style';
import SignalListLayer from "../signal-list-layer/signal-list-layer";
import Settings from "../settings/settings";
import ActivityList from "../activity-list/activity-list";
import {currentUserExists, EventEmitter} from "../util";
import {Coachmark, TeachingBubbleContent} from "office-ui-fabric-react";
import {DirectionalHint} from "office-ui-fabric-react/lib/common/DirectionalHint";


const Main: React.FC = () => {
  const [isSignalListVisible, setSignalListVisibility] = useState(false);
  const [showCoachMark, setCoachMarkVisibility] = useState(false);
  const [DOMChildRef, setDOMChildRef] = useState(React.useRef(null));
  if (!currentUserExists()) return <Redirect push to={Routes.login}/>;
  EventEmitter.subscribeEvent("coachmark",
    (data) => {
      EventEmitter.emitEvent('blink', null);
      setDOMChildRef(data);
      setCoachMarkVisibility(true);
      setTimeout(() => setCoachMarkVisibility(false), 1000);
    }
  );

  return (
    <MainContainer>
      <Header/>
      <Container>
        <Route exact path={Routes.dashboard} component={SimpleDashboard}/>
        <Route exact path={Routes.transfers} component={Transfer}/>
        <Route exact path={Routes.deposits} component={Deposit}/>
        <Route exact path={Routes.billpay} component={BillPay}/>
        <Route exact path={Routes.accounts} component={AccountList}/>
        <Route exact path={Routes.accountDetails + "/:" + ACCOUNT_DETAILS_PARAM} component={ActivityList}/>
        <Route exact path={Routes.settings} component={Settings}/>
        {
          showCoachMark && (
            <Coachmark
              target={DOMChildRef.current}
              positioningContainerProps={{
                directionalHint: DirectionalHint.topCenter,
                doNotLayer: true
              }}
            >
              <TeachingBubbleContent
                headline="Signal Sent"
                hasCloseIcon={true}
                closeButtonAriaLabel="Close"
                onDismiss={() => setCoachMarkVisibility(false)}
              >
                Click on the signal icon below to see its contents
              </TeachingBubbleContent>
            </Coachmark>
          )
        }
      </Container>
      <Footer runAnimation={false} onTouched={() => setSignalListVisibility(true)}/>
      {isSignalListVisible &&
      <SignalListLayer onHideSignals={() => setTimeout(() => setSignalListVisibility(false), 1000)}/>}
    </MainContainer>
  )
};

export default Main;