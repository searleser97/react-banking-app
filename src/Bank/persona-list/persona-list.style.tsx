import {makeStyles} from '@material-ui/core/styles';
import styled from 'styled-components';

export const styles = makeStyles(theme => ({
  link: {
    outline: 'none',
    textDecoration: 'none',
    '&:link': {
      textDecoration: 'none'
    },
    '&:visited': {
      textDecoration: 'none'
    },
    '&:hover': {
      textDecoration: 'none'
    },
    '&:active': {
      textDecoration: 'none'
    }
  },
  stack: {
    width: '100%',
    outline: 'none'
  },
  persona: {
    cursor: 'pointer',
    width: '100%',
    borderBottom: '1px solid lightgray',
    outline: 'none',
    '&:hover': {
      background: 'whitesmoke'
    }
  }
}));

export const ScrollableDiv = styled.div`
  display: flex;
  flex: 0 1 auto;
  overflow-y: auto;
  outline: none;
`;
