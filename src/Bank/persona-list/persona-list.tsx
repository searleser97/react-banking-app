import React from 'react';
import {ScrollableDiv, styles} from './persona-list.style';
import {Stack} from 'office-ui-fabric-react/lib/Stack';
import {Persona, PersonaSize} from 'office-ui-fabric-react/lib/Persona';

export interface Item {
  id: number;
  imageUrl: string;
  imageInitials: string;
  text: string;
  secondaryText: string;
  tertiaryText: string;
  optionalText: string;
  size: number;
}

export interface IPersonaList {
  items: Item[];
  onItemSelected(id: number): void;
}

const PersonaList: React.FC<IPersonaList> = (props) => {
  const classes = styles();

  const listItems = props.items.map((item: Item) =>
      <Persona
        key={'Persona_item_' + item.id}
        className={classes.persona}
        imageUrl={item.imageUrl}
        imageInitials={item.imageInitials}
        text={item.text}
        secondaryText={item.secondaryText}
        tertiaryText={item.tertiaryText}
        optionalText={item.optionalText}
        size={PersonaSize.size100}
        onClick={() => props.onItemSelected(item.id)}/>
  );

  return (
    <ScrollableDiv>
      <Stack className={classes.stack} tokens={{childrenGap: 5}}>
        {listItems}
      </Stack>
    </ScrollableDiv>
  );
};

export default PersonaList;
