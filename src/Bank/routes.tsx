export const BANK_SITE = "/react-banking-app";

export const ACCOUNT_DETAILS_PARAM = "accountId";

export const Routes = {
  main: BANK_SITE,
  login: BANK_SITE + "/login",
  dashboard: BANK_SITE + "/dashboard",
  transfers: BANK_SITE + "/transfers",
  accounts: BANK_SITE + "/accounts",
  deposits: BANK_SITE + "/deposits",
  billpay: BANK_SITE + "/billpay",
  accountDetails: BANK_SITE + "/accounts",
  settings: BANK_SITE + "/settings"
};