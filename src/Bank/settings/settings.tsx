import React, {useState} from 'react';
import {FormContainer, styles} from './settings.style';
import {DefaultButton, PrimaryButton} from 'office-ui-fabric-react';
import {Routes} from "../routes";
import {Redirect} from 'react-router-dom';
import {clearLocalStorage} from "../util";

const Settings: React.FC = () => {
  const [logout, setLogOut] = useState(false);
  const classes = styles();
  if (logout) return <Redirect push to={Routes.login}/>
  return (
    <div className={classes.root}>
      <FormContainer>
        <PrimaryButton text="Log Out" onClick={() => {setLogOut(true);}}/>
        <br/>
        <br/>
        <DefaultButton text="Reset" onClick={() => {clearLocalStorage(); setLogOut(true);}}/>
      </FormContainer>
    </div>
  )
};

export default Settings;