import styled, {keyframes} from 'styled-components';
import {ISwipeKeyFrame} from "./signal-list-layer";

export const swipeUp = keyframes`
  from {
    margin-top: 93vh;
  }
  to {
    margin-top: 13vh;
  }
`;

export const swipeDown = keyframes`
  from {
    margin-top: 13vh;
  }
  to {
    margin-top: 93vh;
  }
`;

export const Container = styled.div<ISwipeKeyFrame>`
  height: 87vh;
  width: inherit;
  margin-top: 93vh;
  position: fixed;
  display: flex;
  flex-direction: column;
  align-items: center;
  align-content: center;
  justify-content: center;
  overflow-y: auto;
  animation-name: ${props => props.swipe === "up" ? swipeUp : swipeDown};
  animation-duration: 500ms;
  animation-delay: 10ms;
  animation-iteration-count: 1;
  animation-fill-mode: both;
`;

export const DetailsListContainer = styled.div`
  background: white;
  display: flex;
  flex-grow: 1;
  width: 100%;
  flex-direction: column;
  overflow-y: auto;
`;