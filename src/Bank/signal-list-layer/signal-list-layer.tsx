import React, {useState} from "react";
import {Container, DetailsListContainer} from './signal-list-layer.style';
import {
  CheckboxVisibility,
  DetailsList,
  GroupHeader,
  IGroup,
  IGroupDividerProps,
  IGroupHeaderProps
} from 'office-ui-fabric-react';
import Footer from "../footer/footer";
import {getAllSignals, getRandomInt, newAppId} from "../util";
import IPISignal from "../PI/IPISignal";

export interface ISignalListLayer {
  onHideSignals: () => void;
}

export interface ISwipeKeyFrame {
  swipe: string;
}

const SignalListLayer: React.FC<ISignalListLayer> = (props) => {

  const signals: IPISignal[] = getAllSignals().reverse();
  let startIndex = 0;
  const groups: IGroup[] = [];
  const flatProperties = [];

  for (const signal of signals) {
    groups.push({
      key: signal.name.replace(' ', '') + '_' + newAppId(),
      name: signal.name + "|" + signal.datetime,
      startIndex: startIndex,
      count: signal.properties.length,
      isCollapsed: true,
    });
    startIndex += signal.properties.length;
    for (const property of signal.properties) {
      flatProperties.push({
        key: property.name + '_' + getRandomInt(1, 1000),
        property_name: property.name,
        value: property.value
      });
    }
  }


  const _columns = [
    {
      key: 'property_name',
      name: 'Name',
      fieldName: 'property_name',
      minWidth: 100,
      maxWidth: 200,
      isResizable: true
    },
    {key: 'Value', name: 'Value', fieldName: 'value', minWidth: 100, maxWidth: 200}
  ];
  const [swipeDirection, setSwipeDirection] = useState("up");

  const onRenderHeader = (props: IGroupDividerProps) => {
    const headerCountStyle = {display: "none"};
    const checkButtonStyle = {display: "none"};
    const _onRenderTitle = (props: IGroupHeaderProps): JSX.Element | null => {
      const {group} = props;

      if (!group) {
        return null;
      }
      const name = group.name.split('|')[0];
      const date = group.name.split('|')[1];
      return (
        <div>
          <div>{name}</div>
          <div>{date}</div>
        </div>
      );
    };
    const onToggleSelectGroup = () => {
      // @ts-ignore
      props.onToggleCollapse(props.group as IGroup);
    };
    return (
      <GroupHeader
        // @ts-ignore
        onRenderTitle={_onRenderTitle}
        // @ts-ignore
        styles={{check: checkButtonStyle, headerCount: headerCountStyle}}
        {...props}
        onToggleSelectGroup={onToggleSelectGroup}
      />
    );
  };
  return (
    <Container swipe={swipeDirection}>
      <Footer runAnimation={true} onTouched={() => {
        props.onHideSignals();
        setSwipeDirection("down")
      }}/>

      <DetailsListContainer>
        <DetailsList
          useReducedRowRenderer={true}
          onShouldVirtualize={(props) => true}
          usePageCache={true}
          items={flatProperties}
          groups={groups}
          columns={_columns}
          // @ts-ignore
          groupProps={{
            showEmptyGroups: true,
            onRenderHeader: onRenderHeader,
          }}
          compact={false}
          checkboxVisibility={CheckboxVisibility.hidden}
        />
      </DetailsListContainer>
    </Container>
  );
};

export default SignalListLayer;