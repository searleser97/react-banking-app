import {makeStyles} from '@material-ui/core/styles';
import styled from 'styled-components';

export const styles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexShrink: 1,
    width: '90%',
    flexDirection: 'column'
  },
  cell: {
    borderWidth: '10px',
    borderColor: 'black'
  },
  paper: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  link: {
    outline: 'none',
    textDecoration: 'none',
    '&:link': {
      textDecoration: 'none'
    },
    '&:visited': {
      textDecoration: 'none'
    },
    '&:hover': {
      textDecoration: 'none'
    },
    '&:active': {
      textDecoration: 'none'
    }
  }
}));

export const PaperContent = styled.div`
    display: flex;
    width: 90%;
    padding-top: 10px;
    padding-bottom: 10px;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    align-content: center;
`;

export const VerticalSpace = styled.div`
  height: 50px;
  width: 100%;
  @media (min-width: 500px) {
    height: 100px;
  }
`;