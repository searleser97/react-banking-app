import React from 'react';
import IconCard from '../icon-card/icon-card';
import Grid from '@material-ui/core/Grid';
import {styles, VerticalSpace} from './simple-dashboard.style';
import {Link} from 'react-router-dom';
import {Routes} from '../routes';
import {PaperContent} from "./simple-dashboard.style";
import {Label} from "office-ui-fabric-react";
import {Paper} from "@material-ui/core";

const SimpleDashboard: React.FC = () => {
  const classes = styles();
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <PaperContent>
          <div style={{display: 'flex'}}>
            Welcome to your mobile banking experience
          </div>
          <div style={{display: 'flex', width: '100%'}}>
            <Label></Label>
          </div>
        </PaperContent>
      </Paper>
      <VerticalSpace/>
      <Grid container spacing={3}>
        <Grid className={classes.cell} item xs={6} sm={6} md={6} lg={6}>
          <Link className={classes.link} to={Routes.accounts}>
            <IconCard iconName="AllCurrency" text="Accounts"/>
          </Link>
        </Grid>
        <Grid className={classes.cell} item xs={6} sm={6} md={6} lg={6}>
          <Link className={classes.link} to={Routes.transfers}>
            <IconCard iconName="Switch" text="Transfer"/>
          </Link>
        </Grid>
        <Grid className={classes.cell} item xs={6} sm={6} md={6} lg={6}>
          <Link className={classes.link} to={Routes.billpay}>
            <IconCard iconName="Money" text="Bill Pay"/>
          </Link>
        </Grid>
        <Grid className={classes.cell} item xs={6} sm={6} md={6} lg={6}>
          <Link className={classes.link} to={Routes.deposits}>
            <IconCard iconName="PreviewLink" text="Deposit"/>
          </Link>
        </Grid>
      </Grid>
    </div>
  );
};

export default SimpleDashboard;
