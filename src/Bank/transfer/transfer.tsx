import React, {useState} from 'react';
import {FormContainer, styles} from './transfer.style';
import {Dropdown, IDropdownOption} from 'office-ui-fabric-react/lib/Dropdown';
import {TextField} from 'office-ui-fabric-react/lib/TextField';
import {Label, PrimaryButton} from 'office-ui-fabric-react';
import {ACCOUNTS_KEY} from '../constants';
import {Icon} from 'office-ui-fabric-react/lib/Icon';
import {addAccountActivityToCurrentUser, EventEmitter, getCurrentUser, saveAndSendSignal} from "../util";
import {TransferEvent} from "../PI/Signals/TransferEvent";

const Transfer: React.FC = () => {
  const classes = styles();

  const buttonRef = React.useRef(null);

  const saveAndSend = (fromAccountType: string, toAccountType: string, amount: number) => {
    EventEmitter.emitEvent('coachmark', buttonRef);
    let signal = new TransferEvent("App", amount, fromAccountType, toAccountType);
    addAccountActivityToCurrentUser(fromAccountType, -amount, "Sent $" + amount + " to " + toAccountType);
    addAccountActivityToCurrentUser(toAccountType, amount, "Received $" + amount + " from " + fromAccountType);
    saveAndSendSignal(signal);
  };

  const userAccounts = getCurrentUser().accounts;
  const fromOptions: IDropdownOption[] = [];
  const toOptions: IDropdownOption[] = [];
  for (let account of userAccounts) {
    if (account.type !== ACCOUNTS_KEY.credit) {
      fromOptions.push({key: account.type, text: account.name + " - $" + account.balance});
    }
    toOptions.push({key: account.type, text: account.name + " - $" + account.balance});
  }

  const [amount, setAmount] = useState('0');
  const [fromAccountType, setFromAccountType] = useState(ACCOUNTS_KEY.checking);
  const [toAccountType, setToAccountType] = useState(ACCOUNTS_KEY.savings);
  return (
    <div className={classes.root}>
      <Label
        style={{padding: '15px', width: '80%', textAlign: 'center', fontSize: '20px', flexDirection: 'column'}}>
        <Icon iconName="switch" style={{fontSize: '70px'}}/>
      </Label>
      <FormContainer>
        <Dropdown
          onChange={(event, option: IDropdownOption | undefined, index) => setFromAccountType(option ? option.key as string : '')}
          placeholder="Select an account" label="From" options={fromOptions}/>
        <Dropdown
          onChange={(event, option: IDropdownOption | undefined, index) => setToAccountType(option ? option.key as string : '')}
          placeholder="Select an account" label="To" options={toOptions}/>
        <TextField onChange={(event, newValue) => {
          setAmount(newValue as string)
        }} type="number" label="Amount"/>
        <br/>
        <br/>
        <div ref={buttonRef} className={classes.buttonContainer}>
          <PrimaryButton className={classes.button} onClick={() => saveAndSend(fromAccountType, toAccountType, Number(amount))}
                         text="Transfer"/>
        </div>
      </FormContainer>
    </div>
  )
};

export default Transfer;