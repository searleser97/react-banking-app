import {ACCOUNTS_KEY, Activity, User} from "./constants";
import IPISignal from "./PI/IPISignal";
import IPIProperty from "./PI/IPIProperty";

declare var analytics: any;

export function getUsers(): User[] {
  const users = localStorage.getItem('users');
  return JSON.parse(users ? users : "[]");
}

export function saveUsers(users: (User | undefined)[]): void {
  localStorage.setItem('users', JSON.stringify(users));
}

export function setCurrentUserId(userId: number) {
  localStorage.setItem('currentUserId', userId.toString());
}

export function saveSignal(signal: IPISignal): IPISignal {
  let signalsAsString = localStorage.getItem('signals');
  let signals: IPISignal[] = JSON.parse(signalsAsString ? signalsAsString : "[]") as IPISignal[];
  signal.properties = [
    ...getCurrentUserDimensions(),
    ...signal.properties
  ];
  signals.push(signal);
  localStorage.setItem('signals', JSON.stringify(signals));
  return signal;
}

export function getAllSignals() {
  const signalsAsString = localStorage.getItem('signals');
  return JSON.parse(signalsAsString ? signalsAsString : "[]") as IPISignal[];
}

export function sendSignal(signal: IPISignal) {
  const data: { [index: string]: (number | string) } = {};
  for (const property of signal.properties) {
    data[property.name] = property.value;
  }
  analytics.track({
    name: signal.name,
    data: data
  });
}

export function getRandomInt(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max + 1 - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

export function clearLocalStorage() {
  localStorage.clear();
}

export function currentUserExists(): boolean {
  return Boolean(localStorage.getItem('currentUserId'));
}

export function getCurrentUserId(): number {
  return Number(localStorage.getItem('currentUserId'));
}

export function getCurrentUser(): User {
  const users: User[] = getUsers();
  return users[getCurrentUserId()];
}

export function getAccountIndexForAccountType(accountType: string): number {
  if (accountType === ACCOUNTS_KEY.checking) return 0;
  else if (accountType === ACCOUNTS_KEY.savings) return 1;
  else if (accountType === ACCOUNTS_KEY.credit) return 2;
  else return -1;
}

export function getCurrentUserBalanceForAccount(accountType: string): number {
  return getCurrentUser().accounts[getAccountIndexForAccountType(accountType)].balance;
}

export function addAccountActivityToCurrentUser(accountType: string, operationCost: number, description: string) {
  const user: User = getCurrentUser();
  const account = user.accounts[getAccountIndexForAccountType(accountType)];
  const balanceForAccount: number = account.balance;
  const activity: Activity = {
    balance: balanceForAccount + operationCost,
    operationCost: operationCost,
    description: description,
    date: new Date().toLocaleString(),
  };
  account.balance = balanceForAccount + operationCost;
  account.history.push(activity);
  const users = getUsers();
  users[getCurrentUserId()] = user;
  saveUsers(users);
}

export function getCurrentUserHistoryForAccount(accountType: string): Activity[] {
  return getCurrentUser().accounts[getAccountIndexForAccountType(accountType)].history;
}

export function getCurrentUserProfileImgUrl(): string {
  return getCurrentUser().profileImg;
}

export function getCurrentUserDimensions(): IPIProperty[] {
  const user: User = getCurrentUser();
  return [
    {name: "AppId", value: getAppId()},
    {name: "Name", value: user.name},
    {name: "Age", value: user.age},
    {name: "City", value: user.city},
    {name: "State", value: user.state},
    {name: "Income", value: user.income},
    {name: "Employment", value: user.employment}
  ];
}

export function newAppId(): string {
  const length = getRandomInt(7, 11);
  let appId = '';
  for (let i = 0; i < length; i++) {
    if (getRandomInt(0, 1)) {
      appId += String.fromCharCode(getRandomInt((i ? '0' : '1').charCodeAt(0), '9'.charCodeAt(0)));
    } else {
      appId += String.fromCharCode(getRandomInt('A'.charCodeAt(0), 'Z'.charCodeAt(0)));
    }
  }
  return appId;
}

export function getAppId(): string {
  if (!localStorage.getItem('appId')) localStorage.setItem('appId', newAppId());
  return localStorage.getItem('appId') as string;
}

export function saveAndSendSignal(signal: IPISignal) {
  sendSignal(saveSignal(signal));
}

export class EventEmitter {
  static events: { [index: string]: any[] } = {};
  static emitEvent = (event: string, data: any): void => {
    if (EventEmitter.events[event]) EventEmitter.events[event].forEach(callback => callback(data));
  };
  static subscribeEvent = (event: string, callback: (data: any) => void): void => {
    if (!EventEmitter.events[event]) EventEmitter.events[event] = [];
    EventEmitter.events[event].push(callback);
  };
}